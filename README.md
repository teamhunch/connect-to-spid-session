# connect-to-spid-session

[![wercker status](https://app.wercker.com/status/758b932a6ba7b9b92fdaa03b08026ac6/m/master "wercker status")](https://app.wercker.com/project/bykey/758b932a6ba7b9b92fdaa03b08026ac6)

Component, store and actions which wraps [SPiD sdk](https://github.com/schibsted/sdk-js) and
provides abstraction level for easier integration with react/fluxible project.

## Installation

```
npm install connect-to-spid-session --save
```

## Usage

* [Quick Start](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/quick-start.md)
* [Save and restore](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/save-and-restore.md)
* [API](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/README.md)




