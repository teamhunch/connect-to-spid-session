'use strict';

var React = require('react');
var hoistNonReactStatics = require('hoist-non-react-statics');
var connectToStores = require('fluxible-addons-react/connectToStores');

function createComponent(Component, opts) {
    var prop = opts.prop || 'session';
    var onSaveAction = opts.onSaveAction || null;
    var onDestroyAction = opts.onDestroyAction || null;

    var SpidSessionHandler = React.createClass({
        displayName: 'SpidSessionHandler',

        contextTypes: {
            executeAction: React.PropTypes.func.isRequired,
        },

        triggerSaveAction: function (session) {
            if (onSaveAction) {
                this.context.executeAction(onSaveAction, { session: session });
            }
        },

        triggerDestroyAction: function (session) {
            if (onDestroyAction) {
                this.context.executeAction(onDestroyAction, { session: session });
            }
        },

        triggerActions: function (oldSession, newSession) {
            if (oldSession && oldSession.status == newSession.status) {
                return;
            }

            if (newSession.status === 'logged_in') {
                this.triggerSaveAction(newSession);
            } else if (newSession.status === 'logged_out') {
                this.triggerDestroyAction(newSession);
            }
        },

        componentWillUpdate: function (nextProps) {
            this.triggerActions(this.props[prop], nextProps[prop]);
        },

        componentDidMount: function () {
            this.triggerActions(null, this.props[prop]);
        },

        render: function () {
            return React.createElement(Component, this.props);
        }
    });

    hoistNonReactStatics(SpidSessionHandler, Component);

    return connectToStores(SpidSessionHandler, ['SpidSessionStore'], function (context) {
        var extraProps = {};
        extraProps[prop] = context.getStore('SpidSessionStore').getSession();
        return extraProps;
    });
}

function connectToSpidSession(Component, opts) {
    // support decorator pattern
    if (arguments.length === 1) {
        opts = arguments[0];

        return function handleHistoryDecorator(componentToDecorate) {
            return createComponent(componentToDecorate, opts);
        };
    }

    return createComponent.apply(null, arguments);
}

module.exports = connectToSpidSession;
