# API Documentation

* [connectToSpidSession](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/connectToSpidSession.md)
* [initializeSpidAction](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/initializeSpidAction.md)
* [logoutAction](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/logoutAction.md)
* [restoreSessionAction](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/restoreSessionAction.md)
* [SpidSessionStore](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/SpidSessionStore.md)
