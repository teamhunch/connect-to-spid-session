# API: `connectToSpidSession`

Higher order component, which wraps
[SpidSessionStore](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/SpidSessionStore.md)
and provide hooks for save and destroy of session.

Can be used in ES5 style:

```
MyComponent = connectToSpidSession(MyComponent, { prop: 'session' });
```

or ES6 as annotation:
```
@connectToSpidSession({ prop: 'session' });

class MyComponent extends Component {

}
```

Supported options:

| Field Name       |  Required?   | Description                                         |
|------------------|--------------|-----------------------------------------------------|
| prop             |      No      |  Name of property which receive session object, defaults to 'session' |
| onSaveAction     |      No      | Reference to fluxible action, it will be triggered when session need to be saved |
| onDestroyAction  |      No      | Reference to fluxible action, it will be triggered when session need to be destroyed |
