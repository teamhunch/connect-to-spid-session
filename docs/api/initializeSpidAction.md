# API: `initializeSpidAction`

`initializeSpidAction` send request to
[SpidSessionStore](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/SpidSessionStore.md)
to initialize SPiD sdk and setup event handlers to keep track on user session.

As payload you can pass all params supported by `init` method of SPiD sdk,
see [here](http://techdocs.spid.no/sdks/javascript/#loading-and-initializing) for more details.

Most important fields:

| Field Name    | Description                             |
|---------------|-----------------------------------------|
| client_id     | Client id. Can be generated using selfservice, like [here](https://stage.payment.schibsted.no/selfservice) |
| server        | Server address, for example `https://payment.schibsted.no/`       |
| prod          | Force production mode when true. Production mode require ssl certificate installed on your server.      |
| logging       | Enable or disable debug. Should be true serverside or crash. |
| session       | Initial session object, should be any non-empty value serverside or crash. |
| cookie        | Should be falsy serverside or crash. |


Example of usage:

```
import { spidActions } from 'connect-to-spid-session';

const SPID_CONFIG =  {
    client_id: 'SPID_CLIENT_ID',
    server: 'SPID_SERVER',
    prod: process.env.NODE_ENV === 'production',
    logging: true,
    session: process.env.BROWSER ? null : 'placeholder',
    cookie: process.env.BROWSER ? true : false
};
...
componentDidMount() {
    this.context.executeAction(spidActions.initializeSpidAction, SPID_CONFIG);
}
```
