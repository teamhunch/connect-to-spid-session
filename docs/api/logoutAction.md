# API: `logoutAction`

Disconnect user with SPiD. `logoutAction` action takes no parameters and dispatches no actions directly.
Change to session will be recorded by
[SpidSessionStore](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/SpidSessionStore.md)
and all components connected to SPiD session will be updated.

Usage example:

```
import { spidActions } from 'connect-to-spid-session';

...

this.context.executeAction(spidActions.logoutAction);
```
