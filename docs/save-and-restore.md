# Save and restore session

You may want to use localstorage or server session to keep track on session during between page reloads.
To do so, in you Application component setup hook-actions:

```
@connectToSpidSession({
  onSaveAction: saveSession,
  onDestroyAction: deleteSession
})
```

Given actions will be triggered on corresponding changes in session state.
Use `saveSession` action to save session to localstorage and `onDestroyAction` to remove it from there.

To use stored session data fire [restoreSessionAction](https://bitbucket.org/teamhunch/connect-to-spid-session/src/master/docs/api/restoreSessionAction.md) with session data as payload.
