module.exports = {
    connectToSpidSession: require('./connectToSpidSession'),
    SpidSessionStore: require('./stores/SpidSessionStore'),
    spidActions: require('./actions/spidActions'),
    VGS: require('./utils/spid-sdk')
};
