var createStore = require('fluxible/addons/createStore');
var VGS = require('../utils/spid-sdk');

var SESSION_CHECK_INTERVAL_MS = 30 * 1000;

var SpidSessionStore = createStore({
    storeName: 'SpidSessionStore',

    handlers: {
        'SPID_INIT': 'onSpidInit',
        'SPID_SESSION_RESTORE': 'onSpidSessionRestore'
    },

    initialize: function () {
        this.status = 'unknown';
        this.data = null;
        this.intervalHandler = null;
    },

    onSpidInit: function (spidConfig) {
        VGS.init(spidConfig);

        VGS.Event.subscribe('auth.login', this._onSessionInit.bind(this));
        VGS.Event.subscribe('auth.logout', this._onSessionEnd.bind(this));
        VGS.Event.subscribe('auth.notLoggedin', this._onSessionEnd.bind(this));

        if (!this.intervalHandler) {
            this.intervalHandler = setInterval(VGS.getLoginStatus, SESSION_CHECK_INTERVAL_MS);
        }
    },

    onSpidSessionRestore: function (sessionData) {
        if (!sessionData || !sessionData.userId) {
            return;
        }

        this.data = sessionData;
        this.status = 'logged_in';

        this.emitChange();
    },

    _onSessionInit: function (data) {
        this.data = data.session;
        this.status = 'logged_in';

        this.emitChange();
    },


    _onSessionEnd: function (/*data*/) {
        this.data = null;
        this.status = 'logged_out';

        this.emitChange();
    },

    getSession: function () {
        return {
            status: this.status,
            data: this.data
        };
    },

    getUserId: function () {
        return this.data ? this.data.userId : null;
    },

    dehydrate: function () {
        return {
            status: this.status,
            data: this.data
        };
    },

    rehydrate: function (state) {
        this.status = state.status;
        this.data = state.data;
    }
});

module.exports = SpidSessionStore;
