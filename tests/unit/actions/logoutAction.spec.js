var chai = require('chai');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
chai.use(sinonChai);
var expect = chai.expect;

var spidActions = require('../../../actions/spidActions');
var logoutAction = spidActions.logoutAction;
var createMockActionContext = require('fluxible/utils/createMockActionContext');

var VGS = require('../../../utils/spid-sdk');

describe('logoutAction', function () {
    var actionContext, logoutStub;

    beforeEach(function () {
        actionContext = createMockActionContext();

        logoutStub = sinon.stub(VGS.Auth, 'logout');
    });

    afterEach(function () {
        logoutStub.restore();
    });

    it('should call VGS.logout', function (done) {
        var payload = {};

        actionContext.executeAction(logoutAction, payload, function (err) {
            if (err) {
                return done(err);
            }

            expect(logoutStub).to.have.been.called;
            done();
        });
    })
});
