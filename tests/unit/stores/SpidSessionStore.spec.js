var chai = require('chai');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
chai.use(sinonChai);
var expect = chai.expect;

var createMockActionContext = require('fluxible/utils/createMockActionContext');
var SpidSessionStore = require('../../../stores/SpidSessionStore');
var hoistNonReactStatics = require('hoist-non-react-statics');
var inherits = require('inherits');

var VGS = require('../../../utils/spid-sdk');

describe('SpidSessionStore', function () {
    var MockedSpidSessionStore, vgsInitStub, vgsEventSubscribeStub, actionContext, callbacks;

    function fireFakeVGSEvent(eventName, data) {
        if (callbacks[eventName]) {
            callbacks[eventName].forEach(function (cb) {
                cb(data);
            });
        }
    }

    beforeEach(function () {
        MockedSpidSessionStore = function (dispatcher) { SpidSessionStore.call(this, dispatcher) };

        inherits(MockedSpidSessionStore, SpidSessionStore);
        hoistNonReactStatics(MockedSpidSessionStore, SpidSessionStore);

        MockedSpidSessionStore.prototype.setSession = function (status, data) {
            this.status = status;
            this.data = data;
            this.emitChange();
        };

        vgsInitStub = sinon.stub(VGS, 'init');

        callbacks = {
            'auth.login': [],
            'auth.logout': [],
            'auth.notLoggedin': []
        };

        vgsEventSubscribeStub = sinon.stub(VGS.Event, 'subscribe', function (eventName, callback) {
            if (callbacks[eventName]) {
                callbacks[eventName].push(callback);
            }
        });

        actionContext = createMockActionContext({
            stores: [MockedSpidSessionStore]
        });
    });

    afterEach(function () {
        vgsInitStub.restore();
        vgsEventSubscribeStub.restore();
    });

    it('should have default value of session', function () {
        var spidSessionStore = actionContext.getStore(SpidSessionStore);
        var savedSession = spidSessionStore.getSession();

        expect(savedSession.status).to.equal('unknown');
        expect(savedSession.data).to.equal(null);
    });

    it('should set logged_in status on login event', function () {
        var sessionData = {
            userId: 5
        };

        var spidConfig = { client_id: 'test' };
        actionContext.dispatch('SPID_INIT', spidConfig);

        fireFakeVGSEvent('auth.login', { session: sessionData });

        var spidSessionStore = actionContext.getStore(SpidSessionStore);
        var savedSession = spidSessionStore.getSession();

        expect(savedSession.status).to.equal('logged_in');
        expect(savedSession.data).to.deep.equal(sessionData);
    });

    it('should set logged_out status on logout event', function () {
        var spidConfig = { client_id: 'test' };
        actionContext.dispatch('SPID_INIT', spidConfig);

        fireFakeVGSEvent('auth.logout', { session: null });

        var spidSessionStore = actionContext.getStore(SpidSessionStore);
        var savedSession = spidSessionStore.getSession();

        expect(savedSession.status).to.equal('logged_out');
        expect(savedSession.data).to.equal(null);
    });

    it('should set logged_out status on notLoggedin event', function () {
         var spidConfig = { client_id: 'test' };
        actionContext.dispatch('SPID_INIT', spidConfig);

        fireFakeVGSEvent('auth.notLoggedin', { session: null });

        var spidSessionStore = actionContext.getStore(SpidSessionStore);
        var savedSession = spidSessionStore.getSession();

        expect(savedSession.status).to.equal('logged_out');
        expect(savedSession.data).to.equal(null);
    });

    describe('getSession', function () {
        it('should return value of status and data fields', function () {
            var sessionData = {
                userId: 5
            };

            var spidSessionStore = actionContext.getStore(SpidSessionStore);
            spidSessionStore.status = 'logged_in';
            spidSessionStore.data = sessionData;

            var savedSession = spidSessionStore.getSession();

            expect(savedSession.status).to.equal('logged_in');
            expect(savedSession.data).to.deep.equal(sessionData);
        });
    });

    describe('getUserId', function () {
        it('should return id of user if logged in', function () {
            var sessionData = {
                userId: 5
            };

            var spidSessionStore = actionContext.getStore(SpidSessionStore);
            spidSessionStore.status = 'logged_in';
            spidSessionStore.data = sessionData;

            var userId = spidSessionStore.getUserId();

            expect(userId).to.equal(sessionData.userId);
        });

        it('should return null if user isn\'t logged in', function () {
            var sessionData = null;

            var spidSessionStore = actionContext.getStore(SpidSessionStore);
            spidSessionStore.status = 'logged_out';
            spidSessionStore.data = sessionData;

            var userId = spidSessionStore.getUserId();

            expect(userId).to.equal(null);
        });
    });

    describe('handlers', function () {
        describe('SPID_INIT', function () {
            it('should pass payload to VGS.init', function () {
                var payload = { client_id: 'test' };
                actionContext.dispatch('SPID_INIT', payload);

                expect(vgsInitStub).to.have.been.calledWith(payload);
            });
        });

        describe('SPID_SESSION_RESTORE', function () {
            it('should save session status and data in store', function () {
                var sessionData = {
                    userId: 1
                };

                actionContext.dispatch('SPID_SESSION_RESTORE', sessionData);

                var spidSessionStore = actionContext.getStore(SpidSessionStore);
                var savedSession = spidSessionStore.getSession();

                expect(savedSession.status).to.equal('logged_in');
                expect(savedSession.data).to.deep.equal(sessionData);
            });

            it('should ignore empty payload', function () {
                var spidSessionStore = actionContext.getStore(SpidSessionStore);
                spidSessionStore.setSession('unknown', null);

                actionContext.dispatch('SPID_SESSION_RESTORE');

                var savedSession = spidSessionStore.getSession();
                expect(savedSession.data).to.equal(null);
            });

            it('should ignore empty object payload', function () {
                var spidSessionStore = actionContext.getStore(SpidSessionStore);
                spidSessionStore.setSession('unknown', null);

                var payload = {};
                actionContext.dispatch('SPID_SESSION_RESTORE', payload);

                var savedSession = spidSessionStore.getSession();
                expect(savedSession.data).to.equal(null);
            });
        });
    });


    describe('dehydrate', function () {
        it('should ignore empty object payload', function () {
            var sessionData = {
                userId: 1
            };

            var spidSessionStore = actionContext.getStore(SpidSessionStore);

            spidSessionStore.setSession('logged_in', sessionData);

            var state = spidSessionStore.dehydrate();
            expect(state.status).to.equal('logged_in');
            expect(state.data).to.deep.equal(sessionData);
        });
    });

    describe('rehydrate', function () {
        it('should ignore empty object payload', function () {
            var sessionData = {
                userId: 1
            };

            var spidSessionStore = actionContext.getStore(SpidSessionStore);

            spidSessionStore.rehydrate({
                status: 'logged_in',
                data: sessionData
            });

            var savedSession = spidSessionStore.getSession();
            expect(savedSession.status).to.equal('logged_in');
            expect(savedSession.data).to.deep.equal(sessionData);
        });
    });
});
