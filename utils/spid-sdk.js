'use strict';

var VGS = require('sdk-js/dist/spid-sdk-1.7.10.js');

if (typeof window !== 'undefined') {
  window.VGS = VGS;
}

module.exports = VGS;
